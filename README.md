# Alloy Discovery Repository

## Abstract

We present an algorithm for the high-throughput computational discovery of intermetallic compounds in systems with a large number of components, focusing on FCC and BCC crystal structures. Our approach combines brute-force structure generation with machine-learning interatomic potentials to efficiently explore potential alloys on and near the convex hull of stable structures.

## Methodology

Our algorithm takes a set of chemical elements and a crystal lattice type (FCC or BCC) as inputs. Key features include:
- **High-throughput Screening**: Capable of evaluating hundreds of thousands of candidate structures per second, per CPU core using a low-rank potential (LRP).
- **Comprehensive Search**: Ensures no configurations are missed, enabling the construction of the full convex hull for the system under consideration.
- **Validation**: Validated on BCC (Nb-W, Nb-Mo-W, V-Nb-Mo-Ta-W) and FCC (Cu-Pt, Cu-Pd-Pt, Cu-Pd-Ag-Pt-Au) systems, discovering 268 new alloys not reported in the AFLOW database.

## Conclusion

This repository hosts the results of our computational discovery method for alloys. We have identified a significant number of new alloys in both FCC and BCC systems, enhancing the understanding of high entropy alloys and their potential applications.

### Data Availability

POSCAR files containing the geometry of all newly discovered structures, as well as the constructed convex hulls for each of the BCC and FCC systems, can be found and downloaded [here](https://gitlab.com/zinkovich/alloydiscovery).

## Acknowledgements

We acknowledge Tatiana S. Kostyuchenko for her contributions to the LRP methodology. This work was supported by the Russian Science Foundation under grant number 23-13-00332.

## Contact

For inquiries about this repository, please contact [viktoriia.zinkovich@skoltech.ru](mailto:viktoriia.zinkovich@skoltech.ru).

